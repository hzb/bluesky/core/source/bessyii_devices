from ophyd import PVPositioner, EpicsSignal, EpicsSignalRO, Device, EpicsMotor
from ophyd import Component as Cpt
from ophyd import FormattedComponent as FCpt
from .axes import AxisTypeA, AxisTypeB, AxisTypeD, AxisTypeEnergize
from ophyd import (PseudoPositioner, PseudoSingle, EpicsMotor)
from ophyd.pseudopos import (pseudo_position_argument,
                             real_position_argument)


# This class can be used for the motorized aperture (SLITS) AU1 and AU3
# Starting from those motors we should define gap and offset as pseudo motors

# BlueSky should follow always the same conventions for the slits: 
# we look downstream and use top/bottom/left/right


class ShutterAU4(EpicsMotor):

    """
    A child of the AU4 class that implements an open and close method
    """ 
     
    def __init__(self, prefix, open_value=15,close_value=0, **kwargs):
        self.open_value = open_value
        self.close_value = close_value
        super().__init__(prefix, **kwargs)

        self.open_value = open_value
        self.close_value = close_value
    

class AU4(Device):

    top = Cpt(ShutterAU4, "M3", labels={"apertures"})
    bottom = Cpt(ShutterAU4, "M1", labels={"apertures"})
    left = Cpt(ShutterAU4, "M2", labels={"apertures"})
    right = Cpt(ShutterAU4, "M0", labels={"apertures"})

    
class AU13(Device):
   
    top         = Cpt(AxisTypeA, '', ch_name='M1', labels={"apertures"})
    bottom      = Cpt(AxisTypeA, '', ch_name='M2', labels={"apertures"})
    left        = Cpt(AxisTypeA, '', ch_name='M3', labels={"apertures"}) # wall in old convention
    right       = Cpt(AxisTypeA, '', ch_name='M4', labels={"apertures"}) #ring in old convention

    
class AU2(Device):
    
    
    top         = Cpt(AxisTypeB,      'PH_2', labels={"apertures"})
    bottom      = Cpt(AxisTypeB,      'PH_3', labels={"apertures"})
    left        = Cpt(AxisTypeB,      'PH_4', labels={"apertures"}) # wall in old convention
    right       = Cpt(AxisTypeB,      'PH_5', labels={"apertures"}) #ring in old convention

# EMIL STXM Horizontal slit
class STXM_HS(Device):
    

    trans   = Cpt(AxisTypeB,      'PH_0', labels={"slit"}) # horizontal translation
    width      = Cpt(AxisTypeB,      'PH_1', labels={"slit"}) # horizontal slitwidth
    b_axis    = Cpt(AxisTypeB,      'PH_3', labels={"slit"}) # beam-axis


# AQUARIUS
class AU1Aquarius(Device):
    _default_read_attrs = ['top.readback', 'bottom.readback', 'left.readback', 'right.readback']
    top         = Cpt(AxisTypeD, 'Top')
    bottom      = Cpt(AxisTypeD, 'Bottom')
    left        = Cpt(AxisTypeD, 'Left') 
    right       = Cpt(AxisTypeD, 'Right') 


# METRIXS
# not in use since end of shutdown august 2020 
class AU1Metrixs(Device):
    _default_read_attrs = ['top.readback', 'bottom.readback', 'left.readback', 'right.readback']
    top         = Cpt(AxisTypeD, 'AUTOPES6L')
    bottom      = Cpt(AxisTypeD, 'AUBOTES6L')
    left        = Cpt(AxisTypeD, 'AUWALLES6L') 
    right       = Cpt(AxisTypeD, 'AURINGES6L') 


#### METRIXS    
# METRIXS Spectrometer AU
class AUspecMETRIXS(Device):
    
    top    = Cpt(EpicsMotor, 'Blade-Up', labels={"aperture"})
    bottom = Cpt(EpicsMotor, 'Blade-Down', labels={"aperture"})
    left   = Cpt(EpicsMotor, 'Blade-Wall', labels={"aperture"})
    right  = Cpt(EpicsMotor, 'Blade-Ring', labels={"aperture"})


#### UE52-SGM
class AU1UE52SGM(Device):
    _default_read_attrs = ['top.readback', 'bottom.readback', 'left.readback', 'right.readback']
    top         = Cpt(AxisTypeD, '0')
    bottom      = Cpt(AxisTypeD, '1')
    left        = Cpt(AxisTypeD, '4') 
    right       = Cpt(AxisTypeD, '5') 

    
class AU1UE52SGM(PseudoPositioner):
    ''' This class implements apertures with four blades (top, bottom, left, right)
    and four pseudo motors (htop,hoffset,vgap,voffset)
    '''

    # The pseudo positioner axes:
    hgap    = Cpt(PseudoSingle)
    vgap    = Cpt(PseudoSingle)
    hoffset = Cpt(PseudoSingle)
    voffset = Cpt(PseudoSingle)

    # The real (or physical) positioners:
    top         = Cpt(AxisTypeD, '0')
    bottom      = Cpt(AxisTypeD, '1')
    right       = Cpt(AxisTypeD, '4')
    left        = Cpt(AxisTypeD, '5')
    
    @pseudo_position_argument
    def forward(self, pseudo_pos):
        '''Run a forward (pseudo -> real) calculation'''
        return self.RealPosition(top    = pseudo_pos.voffset+pseudo_pos.vgap/2,
                                 bottom = pseudo_pos.voffset-pseudo_pos.vgap/2,
                                 right  = pseudo_pos.hoffset+pseudo_pos.hgap/2,
                                 left   = pseudo_pos.hoffset-pseudo_pos.hgap/2
                                 )

    @real_position_argument
    def inverse(self, real_pos):
        '''Run an inverse (real -> pseudo) calculation'''
        return self.PseudoPosition(hgap    = -real_pos.left+real_pos.right,
                                   hoffset = (real_pos.right+real_pos.left)/2,
                                   vgap    = real_pos.top-real_pos.bottom,
                                   voffset = (real_pos.top+real_pos.bottom)/2  
                                   )


#### Energize 
# aperture (W)AU
class AUWEnergize(Device):
    _default_read_attrs = ['top.readback', 'bottom.readback', 'left.readback', 'right.readback']
    top         = Cpt(AxisTypeEnergize, '', ch_name='AT')
    bottom      = Cpt(AxisTypeEnergize, '',ch_name='AB')
    left        = Cpt(AxisTypeEnergize, '',ch_name='AL') 
    right       = Cpt(AxisTypeEnergize, '',ch_name='AR') #Check whether left and right is correct.  

